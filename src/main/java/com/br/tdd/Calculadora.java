package com.br.tdd;

public class Calculadora {

    public static int soma(int primeiroNumero, int segundoNumero){
            int resultado = primeiroNumero + segundoNumero;
            return resultado;
    }

    public static double soma (double primeiroNumero, double segundoNumero){
        double resultado= primeiroNumero + segundoNumero;
        return resultado;
    }

    public static int sub (int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero - segundoNumero;

        return resultado;
    }

    public static double sub (double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero - segundoNumero;

        return resultado;
    }

    public static int divisao(int primeiroNumero, int segundoNumero) throws Exception {
        if(segundoNumero == 0){
            throw new Exception ("Nao existe divisao por zero");
        }else{
            int resultado = primeiroNumero/segundoNumero;
            return resultado;
        }

    }

    //aplicando polimorfismo - reescrevendo o metodo para aceitar entradas diferentes
    public static double divisao(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero/segundoNumero;
        return resultado;
    }

    public static int multiplicacao(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero*segundoNumero;
        return resultado;
    }

    public static double multiplicacao(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero*segundoNumero;
        return resultado;
    }


}
