package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void testarSomaDeDoisNumero (){
        int resultado = Calculadora.soma(2,2);

        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarSomaDeDoisNumeroFlutuantes (){
        double resultado = Calculadora.soma(2.5,2.5);

        Assertions.assertEquals(5, resultado);
    }

    @Test
    public void testarSubDeDoisNumerosInteiros (){
        int resultado = Calculadora.sub (3,2);

        Assertions.assertEquals(1,resultado);
    }

    @Test
    public void testarSubDeDoisNumerosFlutuantes (){
        double resultado = Calculadora.sub(4.5, 3.5);

        Assertions.assertEquals(1, resultado);
    }

    @Test
    public void testarDivisaoDeNumsInteiros () throws Exception {
        int resultado = Calculadora.divisao(4,2);

        Assertions.assertEquals(2,resultado);
    }

    @Test
    public void testarDivisaoPorZero () throws Exception {
        int resultado = Calculadora.divisao(4,0);

        Assertions.assertEquals(2,resultado);
    }


    @Test
    public void testarDivisaoDeNumsFlutuantes (){
        double resultado = Calculadora.divisao(4.5, 2.0);

        Assertions.assertEquals(2.25, resultado);
    }

    @Test
    public void testarMultiplicacaoInteiros (){
        int resultado = Calculadora.multiplicacao (4,5);

        Assertions.assertEquals(20, resultado);
    }

    @Test
    public void testarMultiplicacaoFlutuantes (){
        double resultado= Calculadora.multiplicacao (1.5, 2);

        Assertions.assertEquals(3,resultado);
    }
}
